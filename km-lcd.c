//hd44780 i2c driver

#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/slab.h>
#include <linux/init.h>
#include <linux/i2c.h>
#include <linux/delay.h>
#include <linux/fs.h>
#include <asm/uaccess.h>    //for copy_from_user
#include "lcd.h"

#define DEVICE_NAME "lcd"   //The device will appear at /dev/lcd using this value
#define CLASS_NAME "lcd"    //The device class -- this is a character device driver

#define DRIVER_NAME "lcd"
#define DRIVER_AUTHOR "Taco Eelman <ocatie@gmail.com>"
#define DRIVER_DESC "Single HD44780 i2c display driver"


//functions
static int init_lcd(void);
static void exit_lcd(void);
static int lcd_probe(struct i2c_client *client, const struct i2c_device_id *id);
static int lcd_remove(struct i2c_client *client);
static int messagebuffer_to_lcd(void);
static int lcd_init(struct i2c_client *client);
static int lcd_read(struct i2c_client *client);
static int lcd_setpos(struct i2c_client *client, uint8_t x, uint8_t y);
static int lcd_home(struct i2c_client *client);
static int lcd_putc(struct i2c_client *client, char c);
static int lcd_puts(struct i2c_client *client, char *s);
static int lcd_instruction(struct i2c_client *client, uint8_t ins);
static int lcd_write(struct i2c_client *client, uint8_t val, uint8_t rs, uint8_t rw);
static int lcd_wait_for_bf(struct i2c_client *client);
static int gpio_read(struct i2c_client *client);
static int gpio_write(struct i2c_client *client, uint8_t val);


//----character device declares
static int majorNumber; //by kernel assigned
static char message[256];
static short  size_of_message;
static struct class*  lcdClass  = NULL; //device-driver class struct pointer
static struct device* lcdDevice = NULL; //device-driver device struct pointer

static int dev_open(struct inode *, struct file *);
static int dev_release(struct inode *, struct file *);
static ssize_t dev_read(struct file *, char *, size_t, loff_t *);
static ssize_t dev_write(struct file *, const char *, size_t, loff_t *);

static struct file_operations fops =
{
   .open = dev_open,
   .read = dev_read,
   .write = dev_write,
   .release = dev_release,
};

//----i2c declares
//single client pointer, stored for acces from character device write method
struct i2c_client *theClient = NULL;
struct lcd_device { //can be fetched anytime using i2c_get_clientdata(client)
    struct i2c_client *client;
    //LCD data, can be anything you want
};
//i2c idtable
static struct i2c_device_id lcd_idtable[] = {
    { DRIVER_NAME, 0 },
    { }
};
MODULE_DEVICE_TABLE(i2c, lcd_idtable);
//i2c driver
static struct i2c_driver lcd_i2c_driver = {
    .driver = {
        .owner = THIS_MODULE,
        .name   = DRIVER_NAME
    },

    .id_table   = lcd_idtable,
    .probe      = lcd_probe,
    .remove     = lcd_remove
};



/*
 *  init the lcd driver, called on insmod
 */
static int __init init_lcd(void)
{
    printk(KERN_INFO "Init HD44780 driver.\n");

    //init i2c
    i2c_add_driver(&lcd_i2c_driver);    //register i2c driver

    //initialize fs device
    majorNumber = register_chrdev(0, DEVICE_NAME, &fops);
    if (majorNumber<0){
        printk(KERN_ALERT "Lcd failed to register a major number\n");
        return majorNumber;
    }
    printk(KERN_INFO "Lcd: registered correctly with major number %d\n", majorNumber);

    // Register the device class
    lcdClass = class_create(THIS_MODULE, CLASS_NAME);
    if (IS_ERR(lcdClass)){                // Check for error and clean up if there is
        unregister_chrdev(majorNumber, DEVICE_NAME);
        printk(KERN_ALERT "Failed to register device class\n");
        return PTR_ERR(lcdClass);          // Correct way to return an error on a pointer
    }
    printk(KERN_INFO "Lcd device class registered correctly\n");

    // Register the device driver
    lcdDevice = device_create(lcdClass, NULL, MKDEV(majorNumber, 0), NULL, DEVICE_NAME);
    if (IS_ERR(lcdDevice)){               // Clean up if there is an error
        class_destroy(lcdClass);           // Repeated code but the alternative is goto statements
        unregister_chrdev(majorNumber, DEVICE_NAME);
        printk(KERN_ALERT "Failed to create the device\n");
        return PTR_ERR(lcdDevice);
    }
    printk(KERN_INFO "Lcd device created correctly\n"); // Made it! device was initialized

    return 0;
}

/*
 *  destroy the lcd driver, called on rmmod
 */
static void __exit exit_lcd(void)
{
    printk(KERN_INFO "Quitting HD44780 driver.\n");

    //exit char device
    device_destroy(lcdClass, MKDEV(majorNumber, 0));     // remove the device
    class_unregister(lcdClass);                          // unregister the device class
    class_destroy(lcdClass);                             // remove the device class
    unregister_chrdev(majorNumber, DEVICE_NAME);         // unregister the major number
    printk(KERN_INFO "Lcd character device removed.");

    //exit i2c
    theClient = NULL;
    i2c_del_driver(&lcd_i2c_driver);
}

/*
 * initializa an i2c lcd device
 * called when new i2c device (address) is added
 */
static int lcd_probe(struct i2c_client *client, const struct i2c_device_id *id){
    int res;
    struct lcd_device *lcd;

    if(theClient != NULL){
        printk(KERN_CRIT "Only one lcd device allowed!\n");
        return -ENOMEM;
    }
    theClient = client;

    printk(KERN_INFO "Probe i2c lcd drevice.\n");

    lcd = kzalloc(sizeof(struct lcd_device), GFP_KERNEL);
    if(lcd == NULL) {
        printk(KERN_CRIT "failed to create lcd device!\n");
        return -ENOMEM;
    }

    lcd->client = client;
    i2c_set_clientdata(client, lcd);

    //initialize display
    res = lcd_init(client);
    if(res < 0) return res;

    return 0;
}

/*
 * Remove an i2c device
 * called when i2c device is removed
 */
static int lcd_remove(struct i2c_client *client){
    struct lcd_device *lcd = i2c_get_clientdata(client);

    kfree(lcd);

    if(client == theClient) {
        theClient = NULL;
    }

    return 0;
}

/***************** CHARDEVICE METHODS *****************/

/* function is called each time the device is opened
 *  inodep A pointer to an inode object (defined in linux/fs.h)
 *  filep A pointer to a file object (defined in linux/fs.h)
 */
static int dev_open(struct inode *inodep, struct file *filep){
    printk(KERN_INFO "Lcd device has been opened\n");
    return 0;
}

/* unction that is called whenever the device is closed/released by the userspace program
 * inodep A pointer to an inode object defined in linux/fs.h
 * filep A pointer to a file object defined in linux/fs.h
 */
static int dev_release(struct inode *inodep, struct file *filep){
    printk(KERN_INFO "Lcd device successfully closed\n");
    return 0;
}

//function is called whenever device is being read from user space
static ssize_t dev_read(struct file *filep, char *buffer, size_t len, loff_t *offset){
    int error_count = 0;
    //copy_to_user can acces pointers in userspace
    error_count = copy_to_user(buffer, message, size_of_message);

    if (error_count==0){
        printk(KERN_INFO "Lcd sent %d characters to the user\n", size_of_message);
        return (size_of_message=0);  // clear the position to the start and return 0
    }
    else {
        printk(KERN_INFO "Lcd failed to send %d characters to the user\n", error_count);
        return -EFAULT;
    }

}

//function is called whenever the device is written to from userspace
static ssize_t dev_write(struct file *filep, const char *buffer, size_t len, loff_t *offset){
    int i;
    for (i = 0; i < len; i++) {
        message[i] = buffer[i];
    }
    //sprintf(message, "%s", buffer);
    size_of_message = strlen(message);                 // store the length of the stored message
    printk(KERN_INFO "Lcd received %d %d characters from the user\n", len, size_of_message);
    messagebuffer_to_lcd();
    return len;
}

//write contenct of message buffer to the lcd
static int messagebuffer_to_lcd(){
    if(theClient == NULL)   return 0;

    lcd_home(theClient);
    lcd_puts(theClient, message);
    return 0;
}


/***************** HD44780 METHODS *****************/

//init the lcd hd44780 interface
static int lcd_init(struct i2c_client *client) {
    int res;

    printk(KERN_INFO "LCD init\n");

    //set 4bit interface (see Figure 24 of HD44780 datasheet)
    res = gpio_write(client, ( LCD_PIN_D4 | LCD_PIN_D5 | LCD_PIN_EN ) );
    if(res < 0) return res;
    res = gpio_write(client, ( LCD_PIN_D4 | LCD_PIN_D5 ) );
    if(res < 0) return res;

    usleep_range(4200, 4500);

    res = gpio_write(client, ( LCD_PIN_D4 | LCD_PIN_D5 | LCD_PIN_EN ) );
    if(res < 0) return res;
    res = gpio_write(client, ( LCD_PIN_D4 | LCD_PIN_D5 ) );
    if(res < 0) return res;

    usleep_range(110, 210);

    res = gpio_write(client, ( LCD_PIN_D4 | LCD_PIN_D5 | LCD_PIN_EN ) );
    if(res < 0) return res;
    res = gpio_write(client, ( LCD_PIN_D4 | LCD_PIN_D5 ) );
    if(res < 0) return res;

    res = gpio_write(client, ( LCD_PIN_D5 | LCD_PIN_EN ) );
    if(res < 0) return res;
    res = gpio_write(client, ( LCD_PIN_D5 ) );
    if(res < 0) return res;

    //BF can now be read

    res = lcd_instruction(client,  (
            LCD_FUNCTION_SET
             | LCD_FUNCTION_SET_4BIT
             | LCD_FUNCTION_SET_5x8
             | LCD_FUNCTION_SET_2LINES
    ) );
    //res = lcd_instruction(client,  0x28); //4bit, 2 lines, 5x8 font (function set)
    if(res < 0) return res;

    res = lcd_instruction(client,  (
            LCD_DISPLAY_CONTROL
             | LCD_DISPLAY_CONTROL_BLINK_OFF
             | LCD_DISPLAY_CONTROL_CURSOR_OFF
             | LCD_DISPLAY_CONTROL_DISPLAY_ON
    ) );
    //res = lcd_instruction(client,  0x0F); //display on, cursor on, blink
    if(res < 0) return res;

    res = lcd_instruction(client,  LCD_CLEAR_DISPLAY);
    if(res < 0) return res;

    res = lcd_instruction(client,
            LCD_ENTRY_MODE
             | LCD_ENTRY_MODE_DISPLAY_SHIFT_OFF
             | LCD_ENTRY_MODE_INCREMENT
    );
    //res = lcd_instruction(client,  0x06); //entry mode set (increment, no shift)
    if(res < 0) return res;

    return 0;
}

//set the cursor on the given position
static int lcd_setpos(struct i2c_client *client, uint8_t x, uint8_t y) {
    uint8_t ddram = 0x00;

    if( x <= 16 )   ddram |= x;
    if( y > 0)      ddram |= 0x40;

    return lcd_instruction(client, 0x80 | ddram);
}

//return the cursor to 0,0
static int lcd_home(struct i2c_client *client){
    return lcd_setpos(client, 0, 0);
}

//put a character on the current cursor position
static int lcd_putc(struct i2c_client *client, char c){
    return lcd_write(client, (uint8_t) c, 1, 0);
}

//put a string on the current cursor position
static int lcd_puts(struct i2c_client *client, char *s){
    int res;
    int char_on_line = 0;

    int i;
    for( i = 0; s[i] != '\0'; i++ ) {
        printk(KERN_INFO "%c", s[i]);

        //newline if 16 chars on line
        if(char_on_line > 15){
            lcd_setpos(client, 0, 1);   //set cursor to begin second line
            char_on_line = 0;
        }

        //new line on \n
        if( ( s[i] == '\n' ) ) {

            lcd_setpos(client, 0, 1);   //set cursor to begin second line
            char_on_line = 0;

        } else {  //not a newline

            //write character
            res = lcd_putc(client, s[i]);
            if(res < 0) return res;

            char_on_line++;

        }

        //maximum chars reached
        if(i > LCD_MAX_CHAR_COUNT)   return 0;

    }

    return 0;
}

//TODO
static int lcd_read(struct i2c_client *client) {
    return 0;
}

//send an 8bit instruction to the lcd
static int lcd_instruction(struct i2c_client *client, uint8_t ins) {
    return lcd_write(client, ins, 0, 0);
}

//write 8bit data to lcd and wait for BF to clear
static int lcd_write(struct i2c_client *client, uint8_t val, uint8_t rs, uint8_t rw) {
    int res;

    uint8_t db4 = 0;
    uint8_t db5 = 0;
    uint8_t db6 = 0;
    uint8_t db7 = 0;
    uint8_t _rs = 0;
    uint8_t _rw = 0;

    //determine rs and rw
    if(rs)  _rs = LCD_PIN_RS;
    else    _rs = 0;
    if(rw)  _rw = LCD_PIN_RW;
    else    _rw = 0;

    //write first 4 bits
    if(val & 0x10)  db4 = LCD_PIN_D4;
    else            db4 = 0;
    if(val & 0x20)  db5 = LCD_PIN_D5;
    else            db5 = 0;
    if(val & 0x40)  db6 = LCD_PIN_D6;
    else            db6 = 0;
    if(val & 0x80)  db7 = LCD_PIN_D7;
    else            db7 = 0;

    res = gpio_write(client, ( _rs | _rw | db4 | db5 | db6 | db7 | LCD_PIN_EN ) );
    if(res < 0) return res;
    res = gpio_write(client, ( _rs | _rw | db4 | db5 | db6 | db7 ) );
    if(res < 0) return res;

    //write last 4 bits
    if(val & 0x01)  db4 = LCD_PIN_D4;
    else            db4 = 0;
    if(val & 0x02)  db5 = LCD_PIN_D5;
    else            db5 = 0;
    if(val & 0x04)  db6 = LCD_PIN_D6;
    else            db6 = 0;
    if(val & 0x08)  db7 = LCD_PIN_D7;
    else            db7 = 0;

    res = gpio_write(client, ( _rs | _rw | db4 | db5 | db6 | db7 | LCD_PIN_EN ) );
    if(res < 0) return res;
    res = gpio_write(client, ( _rs | _rw | db4 | db5 | db6 | db7 ) );
    if(res < 0) return res;

    //wait for busy flag
    res = lcd_wait_for_bf(client);
    if(res < 0) return res;

    return 0;
}

//wait for busy flag to become 0
static int lcd_wait_for_bf(struct i2c_client *client) {
    //TODO
    usleep_range(100, 200);
    //usleep_range(2000, 2050);    //for LCD_HOME instruction
    return 0;
}

//write data to lcd pins
static int gpio_write(struct i2c_client *client, uint8_t val) {
    int res = i2c_smbus_write_byte(client, ( val | LCD_PIN_BL) );   //backlight always on
    if(res < 0){
        printk(KERN_CRIT "Error writing data to lcd.\n");
        return res;
    }
    return res;
}

//read data on lcd pins
static int gpio_read(struct i2c_client *client) {
    return i2c_smbus_read_byte(client);
}



module_init(init_lcd);
module_exit(exit_lcd);

MODULE_LICENSE("GPL");
MODULE_AUTHOR(DRIVER_AUTHOR);
MODULE_DESCRIPTION(DRIVER_DESC);
