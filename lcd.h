#ifndef LCD_H
#define LCD_H

#define LCD_MAX_CHAR_COUNT 37   //(2 x 16 chars) + (2 x '\r\n') + '\0' = 37 char's max

//HD44780 pins
#define LCD_PIN_BL 0x08 //backlight
#define LCD_PIN_RS 0x01
#define LCD_PIN_RW 0x02
#define LCD_PIN_EN 0x04
#define LCD_PIN_D4 0x10
#define LCD_PIN_D5 0x20
#define LCD_PIN_D6 0x40
#define LCD_PIN_D7 0x80

//HD44780 instructions
#define LCD_CLEAR_DISPLAY                   0x01
#define LCD_HOME                            0x02
#define LCD_ENTRY_MODE                      0x04
#define LCD_ENTRY_MODE_DISPLAY_SHIFT_ON     0x01
#define LCD_ENTRY_MODE_DISPLAY_SHIFT_OFF    0x00
#define LCD_ENTRY_MODE_INCREMENT            0x02
#define LCD_ENTRY_MODE_DECREMENT            0x00
#define LCD_DISPLAY_CONTROL                 0x08
#define LCD_DISPLAY_CONTROL_BLINK           0x01
#define LCD_DISPLAY_CONTROL_BLINK_OFF       0x00
#define LCD_DISPLAY_CONTROL_CURSOR          0x02
#define LCD_DISPLAY_CONTROL_CURSOR_OFF      0x00
#define LCD_DISPLAY_CONTROL_DISPLAY_ON      0x04
#define LCD_DISPLAY_CONTROL_DISPLAY_OFF     0x00
#define LCD_SHIFT_CONTROL                   0x10
#define LCD_SHIFT_CONTROL_RIGHT             0x04
#define LCD_SHIFT_CONTROL_LEFT              0x00
#define LCD_SHIFT_CONTROL_SHIFT             0x08    //display shift
#define LCD_SHIFT_CONTROL_MOVE              0x00    //cursor move
#define LCD_FUNCTION_SET                    0x20
#define LCD_FUNCTION_SET_5x10               0x04
#define LCD_FUNCTION_SET_5x8                0x00
#define LCD_FUNCTION_SET_2LINES             0x08
#define LCD_FUNCTION_SET_1LINE              0x00
#define LCD_FUNCTION_SET_1LINES             LCD_FUNCTION_SET_1LINE
#define LCD_FUNCTION_SET_8BIT               0x10
#define LCD_FUNCTION_SET_4BIT               0x00
#define LCD_SET_DDRAM                       0x80


#endif //LCD_H